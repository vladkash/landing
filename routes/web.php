<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');
Auth::routes();
Route::get('/register',['middleware'=>'auth'])->name('register');
Route::post('/sendorder','IndexController@sendOrder')->name('sendOrder');
Route::group(['prefix'=>'admin'],function(){

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/features', 'HomeController@getFeatures')->name('features');
    Route::post('/features/{id}', 'HomeController@changeFeatures')->name('changefeature');

    Route::get('/clients', 'HomeController@getClients')->name('clients');
    Route::post('/clients/{id}', 'HomeController@changeClients')->name('changeclient');

    Route::get('/section/{name}', 'HomeController@getSection')->name('section');
    Route::post('/section/{id}', 'HomeController@changeSection')->name('changesection');

    Route::get('/products', 'HomeController@getProducts')->name('products');
    Route::get('/product/{id}', 'HomeController@changeProduct')->name('changeproduct');
    Route::post('/product/{id}', 'HomeController@changeProductRequest')->name('changeproductrequest');
    Route::get('/newproduct', 'HomeController@newProduct')->name('newproduct');
    Route::post('/newproduct', 'HomeController@newProductRequest')->name('newproductrequest');
    Route::post('/deleteproduct/{id}', 'HomeController@deleteProduct')->name('deleteproduct');
});
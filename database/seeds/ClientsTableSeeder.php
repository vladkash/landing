<?php

use Illuminate\Database\Seeder;
class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i=1;$i<4;$i++){
            DB::table('clients')->insert([
                'name'=>'Anna Doe',
                'text'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis doloribus, inventore molestiae obcaecati praesentium saepe? Expedita illo nostrum quia repellat sunt. Architecto earum molestiae odit quis quos saepe vitae voluptas?',
                'img'=>'storage/client'.$i
            ]);
        }
    }
}

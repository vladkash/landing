<?php

use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sections')->insert([
            'name'=>'delivery',
            'title'=>'Информация о доставке',
            'subtitle'=>'Наша доставка - радость клиентов',
            'text'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor harum impedit quibusdam similique ullam! Amet asperiores at eligendi fugit in iste iure, labore libero molestias, nisi odio reprehenderit tenetur voluptatibus!'
        ]);
        DB::table('sections')->insert([
            'name'=>'pay',
            'title'=>'Оплата',
            'subtitle'=>'Платите, как вам удобно',
            'text'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor harum impedit quibusdam similique ullam! Amet asperiores at eligendi fugit in iste iure, labore libero molestias, nisi odio reprehenderit tenetur voluptatibus!'
        ]);
        DB::table('sections')->insert([
            'name'=>'contacts',
            'title'=>'Контакты',
            'text'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, dolore eligendi et fugit laudantium maiores nihil perspiciatis quas quia sapiente tenetur veniam? At, error expedita fugiat maxime nihil voluptates voluptatibus? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, dolore eligendi et fugit laudantium maiores nihil perspiciatis quas quia sapiente tenetur veniam? At, error expedita fugiat maxime nihil voluptates voluptatibus?'
        ]);
    }
}

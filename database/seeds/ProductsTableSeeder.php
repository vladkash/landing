<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i=0; $i<3; $i++){
            DB::table('products')->insert([
                'name' => 'Product Name1',
                'title' => 'Product Title1',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem, optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos. Odit sed qui, dolorum!.',
                'price'=>'500',
                'img'=>'product1.jpg'
            ]);
            DB::table('products')->insert([
                'name' => 'Product Name2',
                'title' => 'Product Title2',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem, optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos. Odit sed qui, dolorum!.',
                'price'=>'500',
                'img'=>'product2.jpg'
            ]);
            DB::table('products')->insert([
                'name' => 'Product Name3',
                'title' => 'Product Title3',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem, optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos. Odit sed qui, dolorum!.',
                'price'=>'500',
                'img'=>'product3.jpg'
            ]);
        }
    }
}

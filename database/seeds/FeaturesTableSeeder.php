<?php

use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('features')->insert([
            'title'=>'Отличное качество',
            'text'=>'Some quick example text to build on the card title and make up the bulk of the card\'s content.Some quick example text to build on the card title and make up the bulk of the card\'s content.',
            'img'=>'case.png'
        ]);
        DB::table('features')->insert([
            'title'=>'Быстрая доставка',
            'text'=>'Some quick example text to build on the card title and make up the bulk of the card\'s content.Some quick example text to build on the card title and make up the bulk of the card\'s content.',
            'img'=>'clocks.png'
        ]);
        DB::table('features')->insert([
            'title'=>'Низкие цены',
            'text'=>'Some quick example text to build on the card title and make up the bulk of the card\'s content.Some quick example text to build on the card title and make up the bulk of the card\'s content.',
            'img'=>'diamond.png'
        ]);

    }
}

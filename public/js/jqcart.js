/*!
 * jQuery jqCart Plugin v1.1.2
 * requires jQuery v1.9 or later
 *
 * http://incode.pro/
 *
 * Date: Date: 2016-05-18 19:15
 */
;(function($) {
  'use strict';
  var cartData,
    itemData,
    orderPreview = '',
    totalCnt = 0,
    visibleLabel = false,
    label = $('<div class="nav-link jqcart-cart-label"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="jqcart-total-cnt">0</span></div>'),
    modal = '<div class="jqcart-layout"><div class="jqcart-checkout">123</div></div>',
    orderform = '';
  var opts = {
		buttons: '.add_item',
		cartLabel: 'body',
		visibleLabel: false,
		openByAdding: false,
        handler: '/',
		currency: '$'
  };
  var actions = {
    init: function(o) {
      opts = $.extend(opts, o);
      cartData = actions.getStorage();
      if (cartData !== null && Object.keys(cartData).length) {
        for (var key in cartData) {
          if (cartData.hasOwnProperty(key)) {
            totalCnt += cartData[key].count;
          }
        }
        visibleLabel = true;
      }
      label.prependTo(opts.cartLabel)[visibleLabel || opts.visibleLabel ? 'show' : 'hide']()
        .on('click', actions.openCart)
        .find('.jqcart-total-cnt').text(totalCnt);
      $(document)
        .on('click', opts.buttons, actions.addToCart)
        .on('click', '.jqcart-layout', function(e) {
          if (e.target === this) {
            actions.hideCart();
          }
        })
          .on('click', '.jqcart-incr', actions.changeAmount)
          .on('input keyup', '.jqcart-amount', actions.changeAmount)
          .on('click', '.jqcart-del-item', actions.delFromCart)
          .on('submit', '.order-form', actions.sendOrder)
          .on('reset', '.jqcart-orderform', actions.hideCart);
      return false;
    },
    addToCart: function(e) {
      e.preventDefault();
      itemData = $(this).data();
			if(typeof itemData.id === 'undefined') {
				console.log('Отсутствует ID товара');
				return false;
			}
      cartData = actions.getStorage() || {};
      if (cartData.hasOwnProperty(itemData.id)) {
        cartData[itemData.id].count++;
      } else {
        itemData.count = 1;
        cartData[itemData.id] = itemData;
      }
      actions.setStorage(cartData);
      actions.changeTotalCnt(1);
      label.show();
			if(opts.openByAdding) {
				actions.openCart();
			}
      return false;
    },
    delFromCart: function() {
      var $that = $(this),
        line = $that.closest('.jqcart-tr'),
        itemId = line.data('id');
      cartData = actions.getStorage();
      actions.changeTotalCnt(-cartData[itemId].count);
      delete cartData[itemId];
      actions.setStorage(cartData);
      line.remove();
      actions.recalcSum();
      return false;
    },
    changeAmount: function() {
      var $that = $(this),
				manually = $that.hasClass('jqcart-amount'),
        amount = +(manually ? $that.val() : $that.data('incr')),
        itemId = $that.closest('.jqcart-tr').data('id');
      cartData = actions.getStorage();
			if(manually) {
      	cartData[itemId].count = isNaN(amount) || amount < 1 ? 1 : amount;
			} else {
				cartData[itemId].count += amount;
			}
      if (cartData[itemId].count < 1) {
        cartData[itemId].count = 1;
      }
			if(manually) {
				$that.val(cartData[itemId].count);
			} else {
      	$that.siblings('input').val(cartData[itemId].count);
			}
      actions.setStorage(cartData);
      actions.recalcSum();
      return false;
    },
    recalcSum: function() {
      var subtotal = 0,
        amount,
        sum = 0,
        totalCnt = 0;
      $('.jqcart-tr').each(function() {
        amount = +$('.jqcart-amount', this).val();
        sum = Math.ceil((amount * $('.jqcart-price', this).text()) * 100) / 100;
        $('.jqcart-sum', this).html(sum + ' ' + opts.currency);
				subtotal = Math.ceil((subtotal + sum) * 100) / 100;
        totalCnt += amount;
      });
      $('.jqcart-subtotal strong').text(subtotal);
      $('.jqcart-total-cnt').text(totalCnt);
      if (totalCnt <= 0) {
				actions.hideCart();
				if(!opts.visibleLabel) {
        	label.hide();
				}
      }
      return false;
    },
    changeTotalCnt: function(n) {
      var cntOutput = $('.jqcart-total-cnt');
      cntOutput.text((+cntOutput.text() + n));
      return false;
    },
    openCart: function() {
      var subtotal = 0,
      cartData = actions.getStorage();
          orderPreview = '<div class="cart-heading"><p class="jqcart-cart-title">Корзина <form  class="jqcart-orderform"><input type="reset" class="btn btn-warning" value="Закрыть" style="float: right"></form></p></div><table class="table jqcart-table-wrapper"><thead class="jqcart-thead"><tr><th>ID</th><th></th><th>Наименование</th><th>Цена</th><th>Количество</th><th>Сумма</th><th></th></tr></thead><tbody class="jqcart-manage-order">';
      var key, sum = 0;
      for (key in cartData) {
        if (cartData.hasOwnProperty(key)) {
					sum = Math.ceil((cartData[key].count * cartData[key].price) * 100) / 100;
					subtotal = Math.ceil((subtotal + sum) * 100) / 100;
					
          orderPreview += '<tr class="jqcart-tr" data-id="' + cartData[key].id + '">';
					orderPreview += '<th scope="row" class="jqcart-small-td">' + cartData[key].id + '</th>';
					orderPreview += '<td class="jqcart-small-td jqcart-item-img"><img src="' + cartData[key].img + '" alt=""></td>';
          orderPreview += '<td>' + cartData[key].title + '</td>';
          orderPreview += '<td class="jqcart-price">' + cartData[key].price + '</td>';
          orderPreview += '<td><span class="jqcart-incr" data-incr="-1">&#8211;</span><input type="text" class="jqcart-amount" value="' + cartData[key].count + '"><span class="jqcart-incr" data-incr="1">+</span></td>';
          orderPreview += '<td class="jqcart-sum">' + sum + ' ' + opts.currency + '</td>';
					orderPreview += '<th class="jqcart-small-td"><span class="jqcart-del-item"> <i class="fa fa-close" aria-hidden="true"></i></span></th>';
          orderPreview += '</tr>';
        }
      }
      orderPreview += '</tbody></table>';
      orderPreview += '<div class="jqcart-subtotal">Итого: <strong>' + subtotal + '</strong> ' + opts.currency + '</div>';
			
        var cartHtml = subtotal ? (orderPreview + orderform) : '<h2 class="jqcart-empty-cart">Корзина пуста</h2>';
      $(modal).appendTo('body').find('.jqcart-checkout').html(cartHtml);
    },
    hideCart: function() {
      $('.jqcart-layout').fadeOut('fast', function() {
            $(this).remove();
        });
      return false;
    },
      sendOrder: function(e) {
          e.preventDefault();
          var $that = $(this);
          if (Object.keys(cartData).length === 0) {
              $.jqCart('openCart');
              setTimeout(actions.hideCart,2000);
              return false;
          }
          $.ajax({
              url: opts.handler,
              type: 'POST',
             /* dataType: 'json',*/
              data: {
                  orderlist: $.param(actions.getStorage()),
                  userdata: $that.serialize()
              },
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              error: function(errors) {
                  $(modal).appendTo('body').find('.jqcart-checkout').html('<h2 class="jqcart-empty-cart">Произошла ошибка, свяжитесь с нами по телефону, указанному в разделе "Контакты"</h2>');
                  console.log(errors);
                  setTimeout(actions.hideCart,4000);
              },
              success: function(resp) {
                  $(modal).appendTo('body').find('.jqcart-checkout').html('<h2 class="jqcart-empty-cart">' + resp + '</h2>');
                  setTimeout(actions.hideCart,2000);
              }
          });
      },
    setStorage: function(o) {
      localStorage.setItem('jqcart', JSON.stringify(o));
      return false;
    },
    getStorage: function() {
      return JSON.parse(localStorage.getItem('jqcart'));
    }
  };
  var methods = {
		clearCart: function(){
			localStorage.removeItem('jqcart');
			label[opts.visibleLabel ? 'show' : 'hide']().find('.jqcart-total-cnt').text(0);
			actions.hideCart();
		},
		openCart: actions.openCart,
		printOrder: actions.printOrder,
		test: function(){
			actions.getStorage();
		}
	};
  $.jqCart = function(opts) {
    if (methods[opts]) {
      return methods[opts].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof opts === 'object' || !opts) {
      return actions.init.apply(this, arguments);
    } else {
      $.error('Метод с именем "' + opts + '" не существует!');
    }
  };
})(jQuery);


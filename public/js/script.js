$("body").on('click', '[href*="#"]', function(e){
    if($(document).width() < 992){
        $('.navbar-toggler-icon').trigger('click');
    }
    var fixed_offset = 100;
    $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 500);
    e.preventDefault();
});
$('#cart-link').click(function () {
    if($(document).width() < 992){
        $('.navbar-toggler-icon').trigger('click');
    }
});
var showall = $('#showall');
showall.click(function () {
   $('.notshow').slideToggle();
    showall.css('display','none');
});
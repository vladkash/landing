<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $userdata;
    public $orderlist;
    public $allprice = 0;

    public function __construct($userdata,$orderlist)
    {
        $this->userdata = $userdata;
        $this->orderlist = $orderlist;
        foreach($orderlist as $product){
            $this->allprice += $product['price'];
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('eduleadsvladekb@yandex.ru')->view('mail');
    }
}

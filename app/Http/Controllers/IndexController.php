<?php

namespace App\Http\Controllers;

use App\Mail\SendOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use View;
use Telegram;
use App\Product;
use App\Client;
use App\Feature;
use App\Section;
class IndexController extends Controller
{
    //
    public function  index(){
        $products = Product::all();
        $clients = Client::all();
        $delayClients = 0;
        $features = Feature::all();
        $delayFeatures = 0;
        $delivery = Section::where('name','delivery')->first();
        $pay = Section::where('name','pay')->first();
        $contacts = Section::where('name','contacts')->first();
        $i = 0;
        return View::make('index',['products'=>$products, 'clients'=>$clients,'delayClients'=>$delayClients,'features'=>$features,'delayFeatures'=>$delayFeatures,'delivery'=>$delivery,'pay'=>$pay,'contacts'=>$contacts,'i'=>$i]);
    }
    public function sendOrder(Request $request){
        $data = $request->all();
        parse_str($data['userdata'],$userdata);
        parse_str($data['orderlist'],$orderlist);
        Mail::to(config('app.order_email'))->send(new SendOrder($userdata,$orderlist));
        $telegramMessage = 'Имя: '.$userdata['name'].'
Email: '.$userdata['email'].'
Телефон: '.$userdata['phone'].'
Товары:';
        $allprice = 0;
        foreach ($orderlist as $id=>$product){
            $telegramMessage .= '
'.$product['title'].' | '.$product['count'].'шт. | '.$product['price'].' рублей';
            $allprice += $product['price'];
        }
        $telegramMessage .='
Всего: '.$allprice.' рублей';
            Telegram::sendMessage([
            'chat_id' => '@coeurjoie',
            'text' => $telegramMessage
        ]);
        return 'Заявка отправлена!';
    }
}

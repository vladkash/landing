<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Feature;
use App\Client;
use App\Section;
use Illuminate\Support\Facades\Storage;
use View;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function getFeatures(){
        $features = Feature::all();
        return View::make('features',['features'=>$features]);
    }
    public function changeFeatures(Request $request,$id){
        $data = $request->all();
        $feature = Feature::find($id);
        $feature->title = $data['title'];
        $feature->text = $data['text'];
        $feature->save();
        return redirect()->back()->with('status','Изменения сохранены');
    }

    public function getClients(){
        $clients = Client::all();
        return View::make('clients',['clients'=>$clients]);
    }
    public function changeClients(Request $request, $id){
        $client = Client::find($id);
        foreach ($request->file() as $file){
            Storage::delete('app/public/'.$client->img);
            $file->move(storage_path('app/public'), htmlentities($file->getClientOriginalName()));
            $client->img = $file->getClientOriginalName();
        }
        $data = $request->all();
        $client->name = $data['name'];
        $client->text = $data['text'];
        $client->save();
        return redirect()->back()->with('status','Изменения сохранены');
    }

    public function getSection($name){
        $section = Section::where('name',$name)->first();
        return View::make('section',['section'=>$section]);
    }

    public function changeSection(Request $request, $id){
        $section = Section::find($id);
        $data = $request->all();
        $section->title = $data['title'];
        $section->subtitle = $data['subtitle'];
        $section->text = $data['text'];
        $section->save();
        return redirect()->back()->with('status','Изменения сохранены');
    }

    public function getProducts(){
        $products = Product::all();
        return View::make('products',['products'=>$products]);
    }

    public function changeProduct($id){
        $product = Product::find($id);
        return View::make('product',['product'=>$product]);
    }

    public function changeProductRequest(Request $request,$id){
        $product = Product::find($id);
        foreach ($request->file() as $file){
            Storage::delete('/app/public/'.$product->img);
            $file->move(storage_path('app/public/'), htmlentities($file->getClientOriginalName()));
            $product->img = $file->getClientOriginalName();
        }
        $data = $request->all();
        $product->name = $data['name'];
        $product->title = $data['title'];
        $product->text = $data['text'];
        $product->price = $data['price'];
        $product->save();
        return redirect()->route('products')->with('status','Изменения сохранены');
    }

    public function newProduct(){
        return View::make('newproduct');
    }
    public function deleteProduct($id){
        $product = Product::find($id);
        $product->delete();
        return redirect()->back()->with('status','Товар удален');
    }
    public function newProductRequest(Request $request){
        $product = new Product;
        foreach ($request->file() as $file){
            $file->move(storage_path('app/public/'), htmlentities($file->getClientOriginalName()));
            $product->img = $file->getClientOriginalName();
        }
        $data = $request->all();
        $product->name = $data['name'];
        $product->title = $data['title'];
        $product->text = $data['text'];
        $product->price = $data['price'];
        $product->save();
        return redirect()->route('products')->with('status','Товар добавлен');
    }
}

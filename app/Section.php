<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Section
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string|null $subtitle
 * @property string $text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Section whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Section extends Model
{
    //
    protected $guarded = [];
}

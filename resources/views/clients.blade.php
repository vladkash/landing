@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Отзывы клиентов</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @foreach($clients as $client)
                            <form action="{{route('changeclient',['id'=>$client->id])}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label style="width: 75%" for="exampleFormControlInput1">Имя клиента
                                        <input type="text" name="name" class="form-control" value="{{$client->name}}"></label>
                                </div>
                                <div class="form-group">
                                    <label style="width: 75%" for="exampleFormControlTextarea1">Отзыв клиента
                                        <textarea class="form-control" name="text" rows="9" cols="100">{{$client->text}}</textarea></label>
                                </div>
                                <img src="/storage/{{$client->img}}" class="mt-3 mb-3" alt="client img">
                                <div class="input-group">
                                    <input  type="file" class="form-control" name="file" id="appendbutton">
                                </div>
                                <input type="submit" class="btn btn-primary mb-5 mt-3" value="Сохранить">
                            </form>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
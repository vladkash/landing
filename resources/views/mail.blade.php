<p style="font-weight: bold; margin-bottom: 10px">
    Имя: {{$userdata['name']}}<br>
    Email: {{$userdata['email']}}<br>
    Телефон: {{$userdata['phone']}}
</p>
<table class="table">
    <thead>
        <tr>
            <th>Название</th>
            <th>Количество</th>
            <th>Цена</th>
        </tr>
    </thead>
    <tbody>
        @foreach($orderlist as $id=>$product)
            <tr>
                <td>{{$product['title']}}</td>
                <td>{{$product['count']}}</td>
                <td>{{$product['price']}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
<p style="font-weight: bold; text-align: right; margin-top: 10px">Всего: {{$allprice}}</p>
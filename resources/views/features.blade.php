@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Преимущества</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @foreach($features as $feature)
                            <form action="{{route('changefeature',['id'=>$feature->id])}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label style="width: 75%" for="exampleFormControlInput1">Заголовок преимущества
                                    <input type="text" name="title" class="form-control" value="{{$feature->title}}"></label>
                                </div>
                                <div class="form-group">
                                    <label style="width: 75%" for="exampleFormControlTextarea1">Текст преимущества
                                    <textarea class="form-control" name="text" rows="9" cols="100">{{$feature->text}}</textarea></label>
                                </div>
                                <input type="submit" class="btn btn-primary mb-5" value="Сохранить">
                            </form>
                         @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Редактирование товара</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            <form action="{{route('changeproductrequest',['id'=>$product->id])}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label style="width: 75%" for="exampleFormControlInput1">Название товара
                                        <input type="text" name="name" class="form-control" value="{{$product->name}}"></label>
                                </div>
                                <div class="form-group">
                                    <label style="width: 75%" for="exampleFormControlInput1">Заголовок товара
                                        <input type="text" name="title" class="form-control" value="{{$product->title}}"></label>
                                </div>
                                <div class="form-group">
                                    <label style="width: 75%" for="exampleFormControlTextarea1">Текст товара
                                        <textarea class="form-control" name="text" rows="9" cols="100">{{$product->text}}</textarea></label>
                                </div>
                                <div class="form-group">
                                    <label style="width: 75%" for="exampleFormControlInput1">Цена товара
                                        <input type="text" name="price" class="form-control" value="{{$product->price}}"></label>
                                </div>
                                <img src="/storage/{{$product->img}}" class="mt-3 mb-3" alt="product img">
                                <div class="input-group">
                                    <input  type="file" class="form-control" name="img" id="appendbutton">
                                </div>
                                <input type="submit" class="btn btn-primary mt-5" value="Сохранить">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Coeur Joie</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/jqcart.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
</head>

<body>
	<header>
		<!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-light shadow-lg fixed-top" style="background-color: #fff">

    <div class="container">
    	<!-- Navbar brand -->
    <a class="navbar-brand" href="{{route('index')}}">
        <img src="{{asset('img/logo.jpg')}}" alt="logo">
    </a>

    <!-- Collapse button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="basicExampleNav">

        <!-- Links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="#order">Заказать</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#features">О нас</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#products">Товары</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#how_we_works">Условия</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#delivery">Доставка</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#clients">Отзывы</a>
            </li>
            <li class="nav-item">
                <div class="label-place" id="cart-link">
                </div>
            </li>
        </ul>

    </div>
    </div>
    <!-- Collapsible content -->

</nav>
<!--/.Navbar-->
	<div class="view" id="intro">
        <div class="mask full-bg-img d-flex align-items-center rgba-black-strong wow fadeIn container-fluid justify-content-center" data-wow-duration="2s">
                <div class="row d-flex pl-lg-5 pr-lg-5 pl-md-0 pr-md-0 justify-content-center">
                    <div class="col-lg-6 text-right">
                            <h1 class="display-3 font-bold white-text mb-2">Нижнее белье Coeur Joie оптом</h1>
                            <h3 class=" font-bold white-text mb-2"> Эксклюзивное нижнее белье высокого качества по доступной цене для вашего бизнеса.</h3>
                    </div>
                    <div class="col-lg-6">
                            <form class="order-form" id="form1">
                                <div class="form-group mt-2">
                                    <label class="white-text" for="exampleInputEmail1">Имя</label>
                                    <input type="text" name="name" class="form-control" placeholder="Введите имя" required>
                                </div>
                                <div class="form-group">
                                    <label class="white-text" for="exampleInputPassword1">E-mail</label>
                                    <input type="email" name="email" class="form-control" aria-describedby="emailHelp" placeholder="Введите email" required>
                                </div>
                                <div class="form-group">
                                    <label class="white-text" for="exampleInputPassword1">Телефон</label>
                                    <input type="text" name="phone" class="form-control"  placeholder="Введите телефон" required>
                                </div>

                                <button type="submit" id="submit-button" class="btn btn-lg btn-block btn-warning mt-5 btn-rounded">Отправить</button>
                            </form>
                    </div>
                </div>
        </div>
	</div>

	</header>
	<hr class="hr-section">

	<main>
        <section id="features">
		<div class="container">
                <h2 class="mb-4 text-center">Наши преимущества</h2>
                <p class="mb-4 text-center">Почему у нас заказывать выгодней, чем у других</p>
                <div class="row d-flex justify-content-center mb-4">
                    @foreach($features as $feature)
                    <div class="col-lg-4 mb-3 d-flex justify-content-center ">
                        <div class="card border border-warning card-feature pb-5 wow bounceInUp" data-wow-delay="{{$delayFeatures +=0.3}}s">
                            <!-- Card image -->
                            <div class="avatar mx-auto white mt-5"><img class="rounded-circle img-fluid" src="{{asset('img/'.$feature->img)}}" alt="Card image cap"></div>
                            <div class="hr-align"><hr class="feature-hr"></div>
                            <!-- Card content -->
                            <div class="card-body">
                                <!-- Title -->
                                <h4 class="card-title text-center">{{$feature->title}}</h4>
                                <!-- Text -->
                                <p class="card-text text-center">{{$feature->text}}</p>
                                <!-- Button -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
        </div>
        </section>
        <hr class="hr-section">

            <section id="products">
                <div class="container">
                    <h2 class="mb-4 mt-5 text-center">Наши товары</h2>
                    <p class="mb-4 text-center">Убедитесь в неповторимом дизайне сами</p>
                    <div class="row d-flex  mb-4">
                        @foreach($products as $product)
                        <div class="col-lg-4  mb-3  justify-content-center @if($i++>8) notshow @endif  product_item">
                            <!-- Card -->
                            <div class="card card-product wow zoomIn">
                                <div class=" card-image mb-3  d-flex" style="background: url({{'/storage/'.$product->img}}) no-repeat center center;">
                                    <!-- Content -->
                                    <div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4 product-info">
                                        <div>
                                            <h5 class="product_name">Дополнительный заголовок</h5>
                                            <h3 class="card-title pt-2"><strong>{{$product->title}}</strong></h3>
                                            <p>{{$product->text}}</p>
                                            <button class="btn btn-warning add_item" data-id="{{$product->id}}" data-title="{{$product->name}}" data-price="{{$product->price}}" data-img="{{asset('img/'.$product->img)}}">Добавить в корзину</button>
                                        </div>
                                    </div>
                                    <!-- Content -->

                                </div>
                                <div class="card-body">
                                    <!-- Title -->
                                    <h4 class="card-title text-center">{{$product->name}}</h4>
                                    <!-- Text -->
                                    <p class="text-center product_name font-weight-bold">{{$product->price}} ₱</p>
                                </div>
                            </div>
                            <!-- Card -->
                        </div>
                        @endforeach
                    </div>
                </div>

            </section>
        <hr class="hr-section">
        <div class="d-flex mt-0 justify-content-center">
            <input type="button" id="showall" class="btn btn-lg  btn-outline-warning waves-effect products_button" value="Показать еще">
        </div>
            <section id="how_we_works">
            <div class="container-fluid">
                <h2 class="mb-4 mt-5 text-center">Как мы работаем</h2>
                <p class="mb-5 text-center">Прозрачная и проверенная система</p>
                <div class="wow bounceInUp">
                    <img src="{{asset('img/how_we_work_dekstop.svg')}}"  class="mt-0 how-img-desktop" alt="how we work">
                    <img src="{{asset('img/how_we_work_mobile.svg')}}"  class="mt-0 how-img-mobile" alt="how we work">
                </div>
            </div>
            </section>
        <hr class="hr-section">

            <section id="delivery">
                <div class="container">
                    <h2 class="mb-4 mt-5 text-center">{{$delivery->title}}</h2>
                    <p class="mb-5 text-center">{{$delivery->subtitle}}</p>
                    <div class="row d-flex justify-content-center mb-4">
                        <div class="col-lg-6 mb-3 d-flex justify-content-center wow fadeIn" data-wow-delay="0.5s" data-wow-duration="1.5s">
                            <p class="delivery-text text-right">{{$delivery->text}}</p>
                        </div>
                        <div class="col-lg-6 mb-3 d-flex justify-content-center wow fadeInLeft">
                            <img src="{{asset('img/delivery.svg')}}" class="delivery-img" alt="">
                        </div>
                    </div>
                </div>
            </section>

        <hr class="hr-section">

            <section id="clients">
                <div class="container">
                    <h2 class="mb-4 mt-5 text-center">Наши клиенты</h2>
                    <p class="mb-5 text-center">Отзывы тех, кто с нами сотрудничает</p>
                    <div class="row d-flex">
                        @foreach($clients as $client)
                        <div class="col-lg-4 mb-5 d-flex justify-content-center">
                            <!-- Card -->
                            <div class="card border-warning card-feature testimonial-card wow bounceInUp" data-wow-delay="{{$delayClients+=0.3}}s">
                                <!-- Avatar -->
                                <div class="avatar mt-5 mx-auto white"><img src="/storage/{{$client->img}}" class="rounded-circle client-img">
                                </div>

                                <div class="card-body">
                                    <!-- Name -->
                                    <h4 class=" text-center card-title">{{$client->name}}</h4>
                                    <hr class="feature-hr">
                                    <!-- Quotation -->
                                    <p class="text-center"><i class="fa fa-quote-left"></i>{{$client->text}}</p>
                                </div>

                            </div>
                            <!-- Card -->
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        <hr class="hr-section">

            <section id="pay">
                <div class="container">
                    <h2 class="mb-4 mt-5 text-center">{{$pay->title}}</h2>
                    <p class="mb-5 text-center">{{$pay->subtitle}}</p>
                    <div class="row d-flex justify-content-center mb-4">
                        <div class="col-lg-6 mb-3 mt-5 d-flex justify-content-center wow fadeIn" data-wow-delay="0.5s">
                            <p class="delivery-text text-right">{{$pay->text}}</p>
                        </div>
                        <div class="col-lg-6 mb-3 d-flex justify-content-center">
                            <div class="container-fluid">
                                <div class="row d-flex justify-content-center wow fadeInRight">
                                    <img style="width: 80%;height: 30%" src="{{asset('img/visa-logo.svg')}}" alt="visa">
                                </div>
                                <div class="row d-flex justify-content-center wow fadeInRight" data-wow-delay="0.3s">
                                    <img style="width: 70%;height: 30%" src="{{asset('img/mastercard-logo.svg')}}" alt="mastercard">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <hr class="hr-section">
            <section id="order">
                <div class="container">
                    <h2 class="mb-4 mt-5 text-center">Заказать</h2>
                    <p class="mb-5 text-center">Станьте нашим клиентом</p>
                    <div class="row d-flex justify-content-center mb-4 pb-5 wow fadeIn">
                        <div class="col-lg-6 d-flex justify-content-center">
                            <img src="{{asset('img/order_image.jpg')}}" alt="order" class="mt-5 order-img">
                        </div>
                        <div class="col-lg-6 d-flex justify-content-center">
                            <form style="width: 80%" class="order-form">
                                <div class="form-group mt-2">
                                    <label for="exampleInputEmail1">Имя</label>
                                    <input type="text" name="name" class="form-control" placeholder="Введите имя" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">E-mail</label>
                                    <input type="email" name="email" class="form-control" aria-describedby="emailHelp" placeholder="Введите email" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Телефон</label>
                                    <input type="text" name="phone" class="form-control" placeholder="Введите телефон" required>
                                </div>

                                <button type="submit" id="submit-button" class="btn btn-lg btn-block btn-warning mt-5 btn-rounded">Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <section id="contacts">
                <div class="container-fluid pt-3 color-block elegant-color">
                    <h2 class="mb-4 mt-5 white-text text-center">{{$contacts->title}}</h2>
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 pt-5 mt-5 mb-5 d-flex justify-content-center">
                            <p class="mb-5 contact-text text-center white-text">{{$contacts->text}}</p>
                        </div>
                        <div class="col-lg-6 mt-5 mb-5 d-flex justify-content-center">
{{--
                            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Ae39c8df82a08c1110835235a256a4d4d2823ee0e27bf696790bcabe4ab617b2c&amp;source=constructor" width="500" height="400" frameborder="0"></iframe>
--}}
                        </div>
                    </div>
                </div>
            </section>
	</main>


    <!-- Footer -->
    <footer class="page-footer font-small cyan color-block warning-color">

        <!-- Footer Links -->

        <!-- Footer Links -->

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© 2018 Copyright
        </div>
        <!-- Copyright -->

    </footer>
    <!-- Footer -->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script src="{{asset('js/script.js')}}"></script>
    <script src="{{asset('js/jqcart.js')}}"></script>
    <script>
        new WOW().init();
        $(function() {
            'use strict';
            // инициализация плагина
            $.jqCart({
                buttons: '.add_item',        // селектор кнопок, аля "Добавить в корзину"
               // handler: '/php/handler.php', // путь к обработчику
                visibleLabel: true,         // показывать/скрывать ярлык при пустой корзине (по умолчанию: false)
                openByAdding: false,         // автоматически открывать корзину при добавлении товара (по умолчанию: false)
                currency: ' рублей',          // валюта: строковое значение, мнемоники (по умолчанию "$")
                cartLabel: '.label-place',
                handler:'{{route('sendOrder')}}'
            });
        });
    </script>
</body>

</html>

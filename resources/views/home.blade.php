@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Выберите редактируемый раздел</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="list-group">
                            <a href="{{route('features')}}" class="list-group-item waves-effect">Преимущества</a>
                            <a href="{{route('products')}}" class="list-group-item waves-effect">Товары</a>
                            <a href="{{route('section',['name'=>'delivery'])}}" class="list-group-item waves-effect">Доставка</a>
                            <a href="{{route('clients')}}" class="list-group-item waves-effect">Клиенты</a>
                            <a href="{{route('section',['name'=>'pay'])}}" class="list-group-item waves-effect">Оплата</a>
                            <a href="{{route('section',['name'=>'contacts'])}}" class="list-group-item waves-effect">Контакты</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

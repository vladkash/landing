@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Редактировать секцию</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            <form action="{{route('changesection',['id'=>$section->id])}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleFormControlInput1" style="width: 75%">Заголовок секции
                                        <input type="text" name="title" class="form-control"  value="{{$section->title}}"></label>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1" style="width: 75%">Подзаголовок секции
                                        <input type="text" name="subtitle" class="form-control" value="{{$section->subtitle}}"></label>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Текст секции
                                        <textarea class="form-control" name="text" rows="9" cols="100">{{$section->text}}</textarea></label>
                                </div>
                                <input type="submit" class="btn btn-primary mb-5" value="Сохранить">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
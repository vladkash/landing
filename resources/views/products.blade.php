@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Товары</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            <a href="{{route('newproduct')}}" class="btn btn-lg btn-block btn-primary mb-3">Добавить новый товар</a>
                        <div class="container">
                            <div class="row d-flex justify-content-center">
                                @foreach($products as $product)
                                    <div class="col-lg-4  mb-3 d-flex justify-content-center">
                                        <div class="card">
                                            <img class="card-img-top" src="/storage/{{$product->img}}" alt="Card image cap">
                                            <div class="card-body">
                                                <h5 class="card-title">{{$product->title}}</h5>
                                                <h4>{{$product->name}}</h4>
                                                <p class="card-text">{{$product->text}}</p>
                                                <p style="font-weight: bold">Цена: {{$product->price}}</p>
                                                <a class="btn btn-primary" href="{{route('changeproduct',['id'=>$product->id])}}">Изменить</a>
                                                <form style="float: right" action="{{route('deleteproduct',['id'=>$product->id])}}" method="post">
                                                    @csrf
                                                    <input type="submit" class="btn btn-danger" value="Удалить">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
